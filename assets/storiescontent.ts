    const storiescontent=[
        {
        
            id: "/api/stories/74052",
            title: "Calinours",
            picture: {
            id: "/api/media_objects/1116574",
            contentURL: "http://s1-betty.ams3.cdn.digitaloceanspaces.com/app/pictures/7c77saq75t79r7mvk3qjsg565e986g6ke6e4d547.jpg"
            }
        
        },
        {
        
            id: "/api/stories/74053",
            title: "Chien bleu",
            picture: {
            id: "/api/media_objects/1116577",
            contentURL: "http://s1-betty.ams3.cdn.digitaloceanspaces.com/app/pictures/99wg5xx6487hf5v62425ebtmva5s5i5ke6e4d547.jpg"
            
        }
        },
        {
        
            id: "/api/stories/74054",
            title: "La chaise bleue",
            picture: {
            id: "/api/media_objects/1116595",
            contentURL: "http://s1-betty.ams3.cdn.digitaloceanspaces.com/app/pictures/37i25a9xfg9a8cf7kr772s89av3uc2s9e6e4d547.jpg"
            }
        }
        ,
        {
        
            id: "/api/stories/74055",
            title: "L'Afrique de Zigomar",
            picture: {
            id: "/api/media_objects/1116599",
            contentURL: "http://s1-betty.ams3.cdn.digitaloceanspaces.com/app/pictures/8496iajsc2d93j8e27j2r35q4pz2q5mfe6e4d547.jpg"
            }
        }
        ,
        {
        
            id: "/api/stories/74057",
            title: "Scritch scratch dip clapote",
            picture: {
            id: "/api/media_objects/1116621",
            contentURL: "http://s1-betty.ams3.cdn.digitaloceanspaces.com/app/pictures/52gr88jwf58sehmtn5sg24v42v5x4279e6e4d547.jpg"
            }
        }
        ,
        {
        
            id: "/api/stories/83339",
            title: "C'est moi le plus fort",
            picture: {
            id: "/api/media_objects/1263525",
            contentURL: "http://s1-betty.ams3.cdn.digitaloceanspaces.com/app/pictures/86b8c6252b822d88e3c8714ce477997ed1d15316.jpg"
            }
        }
        ,
        {
        
            id: "/api/stories/83471",
            title: "Le loup est revenu",
            picture: {
            id: "/api/media_objects/1265551",
            contentURL: "http://s1-betty.ams3.cdn.digitaloceanspaces.com/app/pictures/8fe82ff9ab3010816f756c86ad502f329508ebd2.jpg"
            }
        }
        ,
        {
        
            id: "/api/stories/83476",
            title: "Cornebidouille",
            picture: {
            id: "/api/media_objects/1265669",
            contentURL: "http://s1-betty.ams3.cdn.digitaloceanspaces.com/app/pictures/7a8821feb0775d576012e08f567f826861627f21.jpg"
            }
        }
        ,
        {
        
            id: "/api/stories/83479",
            title: "Une nuit, un chat",
            picture: {
            id: "/api/media_objects/1265679",
            contentURL: "http://s1-betty.ams3.cdn.digitaloceanspaces.com/app/pictures/b6bd38ce79e145dc04bf7fa75a6a0b451b162bb6.jpg"
            }
        }
        ,
        {
        
            id: "/api/stories/83482",
            title: "Loulou",
            picture: {
            id: "/api/media_objects/1265691",
            contentURL: "http://s1-betty.ams3.cdn.digitaloceanspaces.com/app/pictures/3371bdf8913f0597f4a8c45e48e9e4659fb79eb0.jpg"
            }
        }
        ,
        {
        
            id: "/api/stories/226805",
            title: "L'arbre lit",
            picture: {
            id: "/api/media_objects/3440297",
            contentURL: "http://betty-fra.fra1.cdn.digitaloceanspaces.com/app/pictures/6acb4282bfad1520b00297d459e0d0eaaa3cd173.jpg"
            }
        }
        ,
        {
        
            id: "/api/stories/226806",
            title: "Il y avait une maison",
            picture: {
            id: "/api/media_objects/3440299",
            contentURL: "http://betty-fra.fra1.cdn.digitaloceanspaces.com/app/pictures/2cfda1cc918348322d0024ca55d063328d8b496c.jpg"
            }
        }
        ,
        {
        
            id: "/api/stories/241276",
            title: "L'enfant masqué",
            picture: {
            id: "/api/media_objects/3689117",
            contentURL: "http://betty-fra.fra1.cdn.digitaloceanspaces.com/app/pictures/42b504d9c24db31eff8f9510917a31c756db4d19.jpg"
            }
        }
        ,
        {
        
            id: "/api/stories/241278",
            title: "L'escargot merveilleux",
            picture: {
            id: "/api/media_objects/3651052",
            contentURL: "http://betty-fra.fra1.cdn.digitaloceanspaces.com/app/pictures/d34549170f1d9c50ab09a167710207cb9112079f.jpg"
            }
        }
        ,
        {
        
            id: "/api/stories/241279",
            title: "La grotte merveilleuse",
            picture: {
            id: "/api/media_objects/3689118",
            contentURL: "http://betty-fra.fra1.cdn.digitaloceanspaces.com/app/pictures/e134de7ca2c87af652569da109f3c0198077f109.jpg"
            }
        }
        ,
        {
        
            id: "/api/stories/241281",
            title: "Le cartable disparu",
            picture: {
            id: "/api/media_objects/3651064",
            contentURL: "http://betty-fra.fra1.cdn.digitaloceanspaces.com/app/pictures/4f0680e24bcddc1345b253ae1c6f901686a57c3e.jpg"
            }
        }
        ,
        {
        
            id: "/api/stories/241283",
            title: "Le combattant du feu",
            picture: {
            id: "/api/media_objects/3651084",
            contentURL: "http://betty-fra.fra1.cdn.digitaloceanspaces.com/app/pictures/b9e5ec70b7f296c31271dada1c6a96b40f353a5f.jpg"
            }
        }
        ,
        {
        
            id: "/api/stories/241288",
            title: "Le fruit du dragon",
            picture: {
            id: "/api/media_objects/3651089",
            contentURL: "http://betty-fra.fra1.cdn.digitaloceanspaces.com/app/pictures/33d7d325b8979f60ad76d1cec651427f0f447ea5.jpg"
            }
        }
        ,
        {
        
            id: "/api/stories/241289",
            title: "Le gentil tyrannosaure",
            picture: {
            id: "/api/media_objects/3651139",
            contentURL: "http://betty-fra.fra1.cdn.digitaloceanspaces.com/app/pictures/26609950ace123634857770e938bc00a33662904.jpg"
            }
        }
        ,
        {
        
            id: "/api/stories/241292",
            title: "Les enfants et le dragon",
            picture: {
            id: "/api/media_objects/3689119",
            contentURL: "http://betty-fra.fra1.cdn.digitaloceanspaces.com/app/pictures/2f764842926274a5948b40944693530d7a37e8c4.jpg"
            }
        }
        ,
        {
        
            id: "/api/stories/241293",
            title: "Le précieux bazar",
            picture: {
            id: "/api/media_objects/3689093",
            contentURL: "http://betty-fra.fra1.cdn.digitaloceanspaces.com/app/pictures/5a06ed7de8c9c75647f2b6b188d0449c3764eb64.jpg"
            }
        }
        ,
        {
        
            id: "/api/stories/241295",
            title: "Le tipi de l'espace",
            picture: {
            id: "/api/media_objects/3651211",
            contentURL: "http://betty-fra.fra1.cdn.digitaloceanspaces.com/app/pictures/0c9e6e2f53b038141a7544651f8145d99ef4b738.jpg"
            }
        }
        ,
        {
        
            id: "/api/stories/272621",
            title: "L'épouvantail",
            picture: {
            id: "/api/media_objects/4119705",
            contentURL: "http://betty-fra.fra1.cdn.digitaloceanspaces.com/app/pictures/107c3ff9e24537aaf69d7bbbfde7093efe15e376.jpg"
            }
        }
        ,
        {
        
            id: "/api/stories/282245",
            title: "Louise ou l'enfance de Bigoudi",
            picture: {
            id: "/api/media_objects/4260167",
            contentURL: "http://betty-fra.fra1.cdn.digitaloceanspaces.com/app/pictures/9034cc33cc436299d51e5e361d5beb9879049794.jpg"
            }
        }
        ,
        {
        
            id: "/api/stories/282581",
            title: "Test Subscription",
            picture: {
            id: "/api/media_objects/4260753",
            contentURL: "http://betty-fra.fra1.cdn.digitaloceanspaces.com/app/pictures/a611ccfb50567fb647802cf561c2452b9b39e9a6.jpg"
            }
        }
        ,
        {
        
            id: "/api/stories/282592",
            title: "Ma super Story abonnement",
            picture: {
            id: "/api/media_objects/4260619",
            contentURL: "http://betty-fra.fra1.cdn.digitaloceanspaces.com/app/pictures/f95178ba44c0090adea0cf76bd3ae7f2d30384f5.jpeg"
            }
        }
        ,
        {
        
            id: "/api/stories/282594",
            title: "Preview",
            picture: {
            id: "/api/media_objects/4260619",
            contentURL: "http://betty-fra.fra1.cdn.digitaloceanspaces.com/app/pictures/f95178ba44c0090adea0cf76bd3ae7f2d30384f5.jpeg"
            }
        }
        ,
        {
        
            id: "/api/stories/282681",
            title: "Preview abonnement",
            picture: {
            id: "/api/media_objects/4260942",
            contentURL: "http://betty-fra.fra1.cdn.digitaloceanspaces.com/app/pictures/282b6f205df35dff3fff42ff29b90dd69a67e8d6.jpg"
            }
        }
        ,
        {
        
            id: "/api/stories/282817",
            title: "[PREVEW] Apptools Abo",
            picture: {
            id: "/api/media_objects/4261148",
            contentURL: "http://betty-fra.fra1.cdn.digitaloceanspaces.com/app/pictures/6fe334a2765e149d8eb5ef191299c57c031c42f7.jpg"
            }
        }
        ,
        {
        
            id: "/api/stories/282824",
            title: "[PREVEW] Apptools Abo 1233",
            picture: {
            id: "/api/media_objects/4261157",
            contentURL: "http://betty-fra.fra1.cdn.digitaloceanspaces.com/app/pictures/07a1e8da628b5a8cff64ff56631f40ffae65b194.jpg"
            }
        }
        
    ]
    export default storiescontent
    
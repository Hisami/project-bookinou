import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, Button, Image, View, FlatList } from 'react-native';
import { Card } from '@rneui/themed';
import { useEffect, useState } from 'react';
import storiescontent from "./assets/storiescontent";
import axios from "axios";

interface Story {
    id: string,
    title: string,
    picture: {
        id: string
        contentURL: string
    }
}


export default function App() {
    /* pour changement avec Button */
    const [showList, setShowList] = useState(false);
    /* pour données de stories */
    const [data, setData] = useState<Story[]>([]);
    useEffect(() => {
        setData(storiescontent)
    }, []);

    // Pour récupérer des données de console.log(Finalement je n'ai pas pû utiliser ces données et refreshtoken directement )
    const [token, setToken] = useState("");
    const [refreshToken, setRefreshToken] = useState("");
    const [story, setStory] = useState([]);

    useEffect(() => {
        axios.post('https://api-v4.1-dev.mybookinou.com/api/users/login', { "username": "technical_test@bookinou.com", "password": "test69" })
            .then(function (response: any) {
                console.log(response);
                setToken(response.data["hydra:description"])
                setRefreshToken(response.data["refresh_token"])
                console.log(token)
            })
            .catch(function (error: any) {
                console.log(error);
            });

    }, []);

    const config = {
        headers: { Authorization: `Bearer ${token}` }
    };
    const bodyParameters = {
        query: `
    {
    stories {
        edges {
        node {
            id
            title
            picture {
            id
            contentUrl
            }
        }
        }
    }
    }
        `
    };
    axios.post('https://api-v4.1-dev.mybookinou.com/api/graphql', bodyParameters, config)
        .then(function (response: any) {
            console.log(response);
            setStory(response.data.data.stories)
            console.log(story)
        })
        .catch(function (error: any) {
            console.log(error);
        });
    //jusqu'à ici, je n'ai pas utilisé avec les problèmes de durées et expirations et j'ai just récupéré des données  



    // pour rendre des information de story une carte
    type ItemProps = { title: string, contentURL: string };
    const Item = ({ title, contentURL }: ItemProps) => (
        <Card containerStyle={{ alignContent: "center", width: 150, height: 200 }}>
            <Card.Title>{title}</Card.Title>
            <Card.Divider />
            <Card.Image
                style={{ padding: 0, width: 100, height: 100 }}
                source={{
                    uri: contentURL
                }}

            />
        </Card>
    );

    return (
        <View style={styles.container}>
            <View style={styles.container1}>
                <Text style={styles.bigBlue} >Histoires</Text>
                <Text style={styles.textOrange}>Decouvrez votre histoire!!</Text>
                {showList ? <Image
                    source={{ uri: 'https://4.bp.blogspot.com/-dEwF9e446JA/Urlmipc3PvI/AAAAAAAAcHI/vvV20ep6c3s/s160/fukidashi_bw02.png' }}
                    style={{ width: 50, height: 50, margin: 5 }}
                /> : null}
                <Image
                    source={{ uri: 'http://1.bp.blogspot.com/-Ig-D-hIn2UE/UPzH_M-7QbI/AAAAAAAAK0w/PzbFjqzWwBk/s180-c/reading_boy.png' }}
                    style={{ width: 200, height: 200 }}
                />
                <View style={styles.buttonContainer}>
                    <Button title={showList ? "Close" : "show Stories"} color='rgba(111, 202, 186, 1)' onPress={() => { setShowList(!showList) }} />
                </View>
                <StatusBar style="auto" />
                {showList ?
                    <FlatList
                        data={data} horizontal
                        renderItem={({ item }) => <Item title={item.title} contentURL={item.picture.contentURL} />}
                        keyExtractor={item => item.id}
                    />
                    : null}
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    container1: {
        margin: 15,
        alignItems: 'center',
        justifyContent: 'center',
    },
    bigBlue: {
        color: 'orange',
        fontWeight: 'bold',
        fontSize: 30,
        margin: 'auto'
    },
    textOrange: {
        color: 'gray',
        fontWeight: 'normal',
        fontSize: 20,
        margin: 'auto'
    },
    startButton: {

    },
    buttonContainer: {
        height: 100,
        width: 150,
        padding: 10,
        backgroundColor: '#FFFFFF',
        margin: 'auto'
    }
});
